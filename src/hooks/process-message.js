
module.exports = function (options = {}) {
  return async context => {
    const { data } = context;

    if(!data.text) {
      throw new Error('A message must have a text');
    }

    const user = context.params.user;
    const text = context.data.text

    context.data = {
      text,
      userId: user._id,
      createdAt: new Date().getTime()
    };

    return context;
  };
};
