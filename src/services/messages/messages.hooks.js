const {authenticate} = require('@feathersjs/authentication').hooks;
const processMessage = require('../../hooks/process-message');

const admin = require('firebase-admin');

admin.initializeApp({
  credential: admin.credential.cert({
    "project_id": "conny-tech-development",
    "client_email": "firebase-adminsdk-lh8me@conny-tech-development.iam.gserviceaccount.com",
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDM+zXrySfQVdFN\nMV44vq/NwAT/PFNbXe2VRdLANlXx6hIutmJhGTs49jz4gUR5sM1dbl/3tPumrruo\n16zAFIpMTQPVeW0xRXzLZgU+cqyezLRYHzCCXBonALZAc9UgLpO3Pvku7FiY3LCC\nyha7Ery60GFWeM0L7K0I0dD6CVPwro6E9ibzX9ZKsEO3VuMXRF7Ix3SHobs8ZBwz\nvRGm+DHeEI6AVncsNx8/DEkmjW8+hfoArkjgi9BotdGS027zE+tZs9Pca92W3oTW\nr4nNrmCx9IBZ8GK5GrcT4Z3fe/VUbRNqjIPVgTy0Ca9jhLcvpRQfGJpOh4YAPzW3\n0cX3zJRTAgMBAAECggEAEyJB6sQIpOBV3LxGfMqD85NAhCi36Tfv1PbDEPEhfabp\nUpXwwfqOdic6EsJSR3+y8805RRCOSwVVfBHBzN49+JhZysP+ZtKDLyNTSvx+U4Zt\nMSqcJxb80/YzjPmjA9GFNV45+WDTdlVnXpYJoCXEGzYXV/CHx1ys3D1loelb/gPK\nX+b8oyqKYJ3G1+1vhtukE2Vlsq6Qau4VjohLvaa7KpXfcF3hXlEQ6ad5Nj5kkimi\n8/nj8nUv1dnjmhJlXEa/f6ybUISh3/K10my7YvqP8PZigRb8oRw65bm8pu8YWrxK\nw4ZtcWg5v97u6UKYizpN5fmJ3PM4ndHg3kaeehs28QKBgQD4GVtFzfZmAKnLBJyt\nOUOnjuWXZTSbP730GLR9nwDUjdfzHkWy19MZanEIMn3paBXi5ODfCWhKloa30XWN\nzRCmkzVt9hTyC/w11XVmSXpJWRVSnci2CfW0Ul8YTtrFA9mzSw6N1DagRxPhD59i\nkzvpDjLq8qUedVNdhra4Y2cr0QKBgQDTglV0EKtN4l0ItORKJpnA6YQUlCKMlJ3v\nKuuxOMLf98aH1JZ6y8MxzXntlbDfTfcwYSutaLLQ6gcIru7h+mymMuT3RYXrPwhQ\nwbjGsMs0Y1dCrjhiIR8Gu7VZqP7J6/1qjPTJjKKr/R2yB+wIxTfYmDEUVisx3zj7\nW593weWa4wKBgQCkHGnCr+34JSIrl95nu0RkVuLKFXkJ5M0q5RinLAFpmHYrbG/g\nUGFkI4OnT3b4aRBpKMsS3e8tv4pXl2cyKF6GQUQLe8b5ITJxDcT9p0cLs7CmGJcG\ny2nLIN2kf6y4u8ZKkjCLYr/Ln5invCTLGOd9TxYhUeYlUqiVaiH9sEszsQKBgH4d\nN6DGE66T6482dOy1yXXahxPSJCuYrfceq3x3cNMkpW72VmePuaqnby031UfA8EQb\nnHSAeibRdDI8RFnf/Qq4TAgYnzpe0jF84RUv82ogVbZ/tUTlpN9Z608z0gBKQ0dW\nFttajOBqQ1uVF6s/FQaD8LfEjWrmQ860mghJSAOrAoGBAJi9gHQ542TXaeZ07ipp\niYeTLDeqGMBx3A9Uff3jvNTEmBsrlF7DGiYB7rEZTG6roJIG+61TxsMJk8gMjBvi\nMgziZE+huEp28wjVilyz0hKev4BAfpd7+QQnMmy40TITg+SYr2JKWFLLwWO2dkDi\nbRUuH5foq/OwgnoQN2rvt6cf\n-----END PRIVATE KEY-----\n",
  }),
  databaseURL: 'https://conny-tech-development.firebaseio.com'
});

const adminMessaging = () => context => {
  const message = {
    data: {
      title: 'Firebase Messaging',
      body: context.data.text
    },
    topic: 'adminMesaging'
  };

  admin.messaging().send(message)
  .catch((error) => {
    console.log('Failed: ', error)
  })
  .then(console.log('Message sent'))
};

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [
      processMessage(),
      adminMessaging()
    ],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
