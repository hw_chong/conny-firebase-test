const assert = require('assert');
const app = require('../../src/app');

describe('\'messages\' service', () => {
  it('registered the service', () => {
    const service = app.service('messages');

    assert.ok(service, 'Registered the service');
  });
  it('creates user', async () => {
    const user = await app.service('users').create({
      email: 'firebase@test.com',
      password: 'qwe123'
    });

    const params = {
      user
    };
    const message = await app.service('messages').create({
      text: 'firebase test message'
    }, params);

    assert.equal(message.text, 'firebase test message');
    assert.equal(message.userId, user._id);
  });
});
